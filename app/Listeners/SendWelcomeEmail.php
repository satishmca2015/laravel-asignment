<?php

namespace App\Listeners;

use App\Events\UserCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendWelcomeEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
       $user = $event->user;
        $data = array('name'=>$user->name, 'email'=>$user->email);
        Mail::send('mail.signup-email', $data, function($message) use ($user) {
            $message->to('satishmca2015@gmail.com');
            $message->subject('New User Registered');
        });
    }
}
